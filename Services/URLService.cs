using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Google.Protobuf.Collections;
using Service.Url;


namespace App
{
    public class URLService : URLer.URLerBase
    {
        private readonly IUrl _urlUsecase;

        public URLService(IUrl urlUsecase)
        {
            _urlUsecase = urlUsecase;
        }

        public override Task<URLs> ReformURLs(URLs request, ServerCallContext context)
        {
            var reformedUrls = _urlUsecase.ReformUrls(request.Urls);
            var reply = new URLs();
            reply.Urls.AddRange(reformedUrls);

            return Task.FromResult(reply);
        }

        public override Task<PageURLs> MakePageURLs(PageURLs request, ServerCallContext context)
        {
            var parsedPageUrls = parsePageUrls(request.PageUrls);
            var reformedPageUrls = _urlUsecase.ReformPagesUrls(parsedPageUrls);
            var reply = new PageURLs();
            reply.PageUrls.Add(composePageUrls(reformedPageUrls));

            return Task.FromResult(reply);
        }

        private IDictionary<string, IEnumerable<string>> parsePageUrls(MapField<string, URLs> pageUrls)
        {
            var output = new Dictionary<string, IEnumerable<string>>();
            foreach (var (pageUrl, inPageUrls) in pageUrls)
            {
                output.Add(pageUrl, inPageUrls.Urls.ToList());
            }

            return output;
        }

        private MapField<string, URLs> composePageUrls(IDictionary<string, IEnumerable<string>> pageUrls)
        {
            var output = new MapField<string, URLs>();
            foreach (var (pageUrl, inPageUrls) in pageUrls)
            {
                var urls = new URLs();
                urls.Urls.AddRange(inPageUrls);
                output.Add(pageUrl, urls);
            }

            return output;
        }
    }
}