using System;
using System.Collections.Generic;

namespace App
{
    public interface IUrl
    {
        IEnumerable<string> ReformUrls(IEnumerable<string> urls);
        IDictionary<string, IEnumerable<string>> ReformPagesUrls(IDictionary<string, IEnumerable<string>> pagesUrls);
    }
}