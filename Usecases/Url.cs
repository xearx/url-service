using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


namespace App
{
    public class Url : IUrl
    {
        public Url() { }

        public IEnumerable<string> ReformUrls(IEnumerable<string> urls)
        {
            return urls.Select((url) => new Uri(url).AbsoluteUri);
        }

        public IDictionary<string, IEnumerable<string>> ReformPagesUrls(IDictionary<string, IEnumerable<string>> pagesUrls)
        {
            var output = new Dictionary<string, IEnumerable<string>>();
            foreach (var pageUrl in pagesUrls)
            {
                output.Add(pageUrl.Key, ReformPageUrls(pageUrl.Key, pageUrl.Value));
            }

            return output;
        }

        private IEnumerable<string> ReformPageUrls(string pageURI, IEnumerable<string> urls)
        {
            return urls.Select((url) =>
            {
                if (IsUrlAbsolute(url))
                {
                    return url;
                }
                return new Uri(new Uri(pageURI), new Uri(url, UriKind.Relative)).AbsoluteUri;
            });
        }

        private bool IsUrlAbsolute(string url)
        {
            return new Uri(url, UriKind.RelativeOrAbsolute).IsAbsoluteUri;
        }
    }
}